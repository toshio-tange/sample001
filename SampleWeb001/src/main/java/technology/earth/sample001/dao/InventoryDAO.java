package technology.earth.sample001.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import technology.earth.sample001.entity.Inventory;
import technology.earth.sample001.entity.InventoryPK;

/**
 *在庫マスタ
 *
 * @author Owner
 *
 */
@Repository
@Transactional(readOnly = false)
public class InventoryDAO {

	/**
	 * Entity Manager
	 */
	@Autowired
	private EntityManager entityManager;

	/**
	 * 在庫確認
	 * @param productCode
	 * @param storage
	 * @return
	 */
	public Inventory findByProductStorage(String productCode, String storageLocCode) {
		try {
			String sql = "Select e from " + Inventory.class.getName() + " e " //
					+ " Where e.id.productCode = :productCode and e.id.storageLocCode = :storageLocCode";

			Query query = entityManager.createQuery(sql, Inventory.class);
			query.setParameter("productCode", productCode);
			query.setParameter("storageLocCode", storageLocCode);

			return (Inventory) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	/**
	 *  在庫検索
	 * @param productCode
	 * @return 在庫
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Inventory> findByProductCode(String productCode) {
		try {
			String sql = "Select e from " + Inventory.class.getName() + " e " //
					+ " Where e.id.productCode = :productCode ";

			Query query = entityManager.createQuery(sql, Inventory.class);
			query.setParameter("productCode", productCode);

			return query.getResultList();
		} catch (NoResultException e) {
			return new ArrayList();
		}
	}

	/**
	 *  在庫検索
	 * @param storageLocCode
	 * @return 在庫
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Inventory> findByStorageLoc(String storageLocCode) {
		try {
			String sql = "Select e from " + Inventory.class.getName() + " e " //
					+ " Where e.id.storageLocCode = :storageLocCode ";

			Query query = entityManager.createQuery(sql, Inventory.class);
			query.setParameter("storageLocCode", storageLocCode);

			return query.getResultList();
		} catch (NoResultException e) {
			return new ArrayList();
		}
	}

	/**
	 * 在庫一覧
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Inventory> findAll() {
		try {
			String sql = "Select e from " + Inventory.class.getName() + " e order by e.id.productCode,e.id.storageLocCode ";

			Query query = entityManager.createQuery(sql, Inventory.class);

			return query.getResultList();
		} catch (NoResultException e) {
			return new ArrayList();
		}
	}

	/**
	 * 存在確認
	 * @param productCode
	 * @return
	 */
	public boolean exist(String productCode, String storageLocCode) {
		Inventory mp = this.findByProductStorage(productCode, storageLocCode);
		return mp != null;
	}

	/**
	 * 存在しなければ新規作成、存在すれば内容更新
	 * @param productCode
	 * @param storageLocCode
	 * @param quantity
	 * @param user
	 */
	public void upsert(String productCode, String storageLocCode, int quantity, String user) {
		if (!this.exist(productCode, storageLocCode)) {
			this.persist(productCode, storageLocCode, quantity, user);
		} else {
			this.merge(productCode, storageLocCode, quantity, user);
		}
	}

	/**
	 * 新規作成
	 * @param productCode 商品コード
	 * @param storageLocCode 保管場所コード
	 * @param quantity 数量
	 * @param user
	 */
	public void persist(String productCode, String storageLocCode, int quantity, String user) {
		Inventory mp = new Inventory();
		InventoryPK id = new InventoryPK();
		id.setProductCode(productCode);
		id.setStorageLocCode(storageLocCode);
		mp.setId(id);
		Timestamp now = new Timestamp(System.currentTimeMillis());
		mp.setArrivalStatus("ST1");
		mp.setQuantity(quantity);
		mp.setCreateAt(now);
		mp.setUpdateAt(now);
		mp.setCreateUser(user);
		mp.setUpdateUser(user);
		entityManager.persist(mp);
	}

	/**
	 * 更新
	 * @param productCode 商品コード
	 * @param storageLocCode 保管場所コード
	 * @param quantity 数量
	 * @param user
	 */
	public void merge(String productCode, String storageLocCode, int quantity, String user) {
		Inventory mp = this.findByProductStorage(productCode, storageLocCode);
		Timestamp now = new Timestamp(System.currentTimeMillis());
		mp.setQuantity(quantity);
		mp.setUpdateAt(now);
		mp.setUpdateUser(user);
		entityManager.merge(mp);
	}
}
