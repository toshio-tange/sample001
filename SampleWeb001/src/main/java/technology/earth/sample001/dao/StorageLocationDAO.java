package technology.earth.sample001.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import technology.earth.sample001.entity.StorageLocation;

/**
 *保管場所マスタ
 *
 * @author Owner
 *
 */
@Repository
@Transactional(readOnly = false)
public class StorageLocationDAO {

	/**
	 * Entity Manager
	 */
	@Autowired
	private EntityManager entityManager;

	/**
	 *  保管場所検索
	 * @param productName
	 * @return 保管場所
	 */
	public StorageLocation findByStorageLocName(String storageLocName) {
		try {
			String sql = "Select e from " + StorageLocation.class.getName() + " e " //
					+ " Where e.storageLocName = :storageLocName ";

			Query query = entityManager.createQuery(sql, StorageLocation.class);
			query.setParameter("storageLocName", storageLocName);

			return (StorageLocation) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	/**
	 *  保管場所検索
	 * @param productCode
	 * @return 保管場所
	 */
	public StorageLocation findByStorageLocCode(String storageLocCode) {
		try {
			String sql = "Select e from " + StorageLocation.class.getName() + " e " //
					+ " Where e.storageLocCode = :storageLocCode ";

			Query query = entityManager.createQuery(sql, StorageLocation.class);
			query.setParameter("storageLocCode", storageLocCode);

			return (StorageLocation) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	/**
	 * 保管場所一覧
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<StorageLocation> findAll() {
		try {
			String sql = "Select e from " + StorageLocation.class.getName() + " e order by e.storageLocCode ";

			Query query = entityManager.createQuery(sql, StorageLocation.class);

			return query.getResultList();
		} catch (NoResultException e) {
			return new ArrayList();
		}
	}

	/**
	 * 存在確認
	 * @param code
	 * @return
	 */
	public boolean exist(String code) {
		StorageLocation sl = this.findByStorageLocCode(code);
		return sl != null;
	}

	/**
	 * 存在しなければ新規作成、存在すれば内容更新
	 * @param code
	 * @param name
	 * @param user
	 */
	public void upsert(String code, String name, String user) {
		if (!this.exist(code)) {
			this.persist(code, name, user);
		} else {
			this.merge(code, name, user);
		}
	}

	/**
	 * 新規作成
	 *
	 * @param code 保管場所コード
	 * @param name 保管場所名
	 * @param user 操作ユーザ
	 */
	public void persist(String code, String name, String user) {
		StorageLocation mp = new StorageLocation();
		mp.setStorageLocName(name);
		mp.setStorageLocCode(code);
		Timestamp now = new Timestamp(System.currentTimeMillis());
		mp.setCreateAt(now);
		mp.setUpdateAt(now);
		mp.setCreateUser(user);
		mp.setUpdateUser(user);
		entityManager.persist(mp);
	}

	/**
	 * 更新
	 *
	 * @param code 保管場所コード
	 * @param name 保管場所名
	 * @param user 操作ユーザ
	 */
	public void merge(String code, String name, String user) {
		StorageLocation mp = this.findByStorageLocCode(code);
		mp.setStorageLocName(name);
		mp.setStorageLocCode(code);
		Timestamp now = new Timestamp(System.currentTimeMillis());
		mp.setUpdateAt(now);
		mp.setUpdateUser(user);
		entityManager.merge(mp);
	}
}
