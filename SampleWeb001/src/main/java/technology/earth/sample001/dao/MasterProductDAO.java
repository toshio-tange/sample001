package technology.earth.sample001.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import technology.earth.sample001.entity.MasterProduct;

/**
 *商品マスタ
 *
 * @author Owner
 *
 */
@Repository
@Transactional(readOnly = false)
public class MasterProductDAO {

	/**
	 * Entity Manager
	 */
	@Autowired
	private EntityManager entityManager;

	/**
	 * 商品一覧
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<MasterProduct> findAll() {
		try {
			String sql = "Select e from " + MasterProduct.class.getName() + " e order by e.productCode ";

			Query query = entityManager.createQuery(sql, MasterProduct.class);

			return query.getResultList();
		} catch (NoResultException e) {
			return new ArrayList();
		}
	}

	/**
	 *  商品検索
	 * @param productName
	 * @return 商品
	 */
	public MasterProduct findByProductName(String productName) {
		try {
			String sql = "Select e from " + MasterProduct.class.getName() + " e " //
					+ " Where e.productName = :productName ";

			Query query = entityManager.createQuery(sql, MasterProduct.class);
			query.setParameter("productName", productName);

			return (MasterProduct) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	/**
	 *  商品検索
	 * @param productCode
	 * @return 商品
	 */
	public MasterProduct findByProductCode(String productCode) {
		try {
			String sql = "Select e from " + MasterProduct.class.getName() + " e " //
					+ " Where e.productCode = :productCode ";

			Query query = entityManager.createQuery(sql, MasterProduct.class);
			query.setParameter("productCode", productCode);

			return (MasterProduct) query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	/**
	 * 存在確認
	 * @param code
	 * @return
	 */
	public boolean exist(String code) {
		MasterProduct mp = this.findByProductCode(code);
		return mp != null;
	}

	/**
	 * 存在しなければ新規作成、存在すれば内容更新
	 * @param code
	 * @param name
	 * @param user
	 */
	public void upsert(String code, String name, String user) {
		if (!this.exist(code)) {
			this.persist(code, name, user);
		} else {
			this.merge(code, name, user);
		}
	}

	/**
	 * 新規作成
	 *
	 * @param code 商品コード
	 * @param name 商品名
	 * @param user 操作ユーザ
	 */
	public void persist(String code, String name, String user) {
		MasterProduct mp = new MasterProduct();
		mp.setProductName(name);
		mp.setProductCode(code);
		Timestamp now = new Timestamp(System.currentTimeMillis());
		mp.setEffectiveStartDate(now);
		mp.setEffectiveFinishDate(now);
		mp.setCreateAt(now);
		mp.setUpdateAt(now);
		mp.setCreateUser(user);
		mp.setUpdateUser(user);
		entityManager.persist(mp);
	}

	/**
	 * 更新
	 *
	 * @param code 商品コード
	 * @param name 商品名
	 * @param user 操作ユーザ
	 */
	public void merge(String code, String name, String user) {
		MasterProduct mp = this.findByProductCode(code);
		mp.setProductName(name);
		mp.setProductCode(code);
		Timestamp now = new Timestamp(System.currentTimeMillis());
		mp.setUpdateAt(now);
		mp.setUpdateUser(user);
		entityManager.merge(mp);
	}
}
