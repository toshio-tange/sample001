package technology.earth.sample001.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import technology.earth.sample001.dao.InventoryDAO;
import technology.earth.sample001.entity.Inventory;

/**
 * 在庫
 * @author Owner
 *
 */
@Service
public class InventoryService {

	@Autowired
	private InventoryDAO iventoryDAO;

	/**
	 * 全件取得
	 * @return
	 */
	public List<Inventory> findAll() {
		return this.iventoryDAO.findAll();
	}

	/**
	 * 商品コードで検索を行う
	 * @param code
	 * @return
	 */
	public List<Inventory> findByProductCode(String code) {
		return this.iventoryDAO.findByProductCode(code);
	}

	/**
	 *保管場所コードで検索を行う
	 * @param code
	 * @return
	 */
	public List<Inventory> findByStorageLoc(String code) {
		return this.iventoryDAO.findByStorageLoc(code);
	}
	/**
	 * 
	 * @param productCode
	 * @param storageLocCode
	 * @param quantity
	 * @param user
	 */
	public void updateInventory(String productCode, String storageLocCode, int quantity, String user) {
		this.iventoryDAO.upsert(productCode, storageLocCode, quantity, user);
	}

}
