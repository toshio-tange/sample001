package technology.earth.sample001.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import technology.earth.sample001.dao.MasterProductDAO;
import technology.earth.sample001.entity.MasterProduct;

/**
 * 商品マスター
 * @author Owner
 *
 */
@Service
public class MasterProductService {

	@Autowired
	private MasterProductDAO masterProductDAO;

	/**
	 * 全件取得
	 * @return
	 */
	public List<MasterProduct> findAll() {
		return this.masterProductDAO.findAll();
	}

	/**
	 * コード・名前のMapを返す
	 * @return
	 */
	public Map<String, String> getProductMap() {
		List<MasterProduct> list = this.masterProductDAO.findAll();
		Map<String, String> ret = new HashMap<String, String>();
		for (MasterProduct item : list) {
			ret.put(item.getProductCode(), item.getProductName());
		}
		return ret;
	}

	/**
	 * 商品名で検索する
	 * @param name
	 * @return
	 */
	public MasterProduct findByProductName(String name) {
		return this.masterProductDAO.findByProductName(name);
	}

	/**
	 * 商品コードで検索を行う
	 * @param code
	 * @return
	 */
	public MasterProduct findByProductCode(String code) {
		return this.masterProductDAO.findByProductCode(code);
	}

	/**
	 * 新規作成・更新を行う。
	 * @param code
	 * @param name
	 * @param user
	 */
	public void updateProduct(String code, String name, String user) {
		this.masterProductDAO.upsert(code, name, user);
	}
}
