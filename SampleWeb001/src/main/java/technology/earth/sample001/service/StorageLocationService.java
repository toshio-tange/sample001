package technology.earth.sample001.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import technology.earth.sample001.dao.StorageLocationDAO;
import technology.earth.sample001.entity.StorageLocation;

/**
 * 保管場所
 * @author Owner
 *
 */
@Service
public class StorageLocationService {

	@Autowired
	private StorageLocationDAO storageLocationDAO;

	/**
	 * 全件取得
	 *
	 * @return
	 */
	public List<StorageLocation> findAll() {
		return this.storageLocationDAO.findAll();
	}

	/**
	 *  コード・名前のMapを返す
	 * @return
	 */
	public Map<String, String> getStorageMap() {
		List<StorageLocation> list = this.storageLocationDAO.findAll();
		Map<String, String> ret = new HashMap<String, String>();
		for (StorageLocation item : list) {
			ret.put(item.getStorageLocCode(), item.getStorageLocName());
		}
		return ret;
	}

	/**
	 * 保管場所で検索を行う
	 * @param name
	 * @return
	 */
	public StorageLocation findByStorageLocName(String name) {
		return this.storageLocationDAO.findByStorageLocName(name);
	}

	/**
	 * 保管場所コードで検索を行う
	 * @param code
	 * @return
	 */
	public StorageLocation findByStorageLocCode(String code) {
		return this.storageLocationDAO.findByStorageLocCode(code);
	}

	/**
	 * 新規作成・更新を行う。
	 * @param code
	 * @param name
	 * @param user
	 */
	public void updateStorageLoc(String code, String name, String user) {
		this.storageLocationDAO.upsert(code, name, user);
	}
}
