package technology.earth.sample001.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import technology.earth.sample001.urls.Urls;

@Controller
public class WelcomeController {

	@RequestMapping(value = { Urls.URL_ROOT_URL, Urls.URL_HOME_URL, Urls.URL_WELCOME_URL }, method = RequestMethod.GET)
	public String welcomePage(Model model) {
		model.addAttribute("title", "ようこそ");
		model.addAttribute("message", "アーステクノロジーです");
		return "home";
	}

}