package technology.earth.sample001.controller;

import java.security.Principal;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import technology.earth.sample001.urls.Urls;
import technology.earth.sample001.utils.WebUtils;

@Controller
public class MainController {
	/**
	 * adminページ
	 *
	 * @param model
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = Urls.URL_ADMIN_URL, method = RequestMethod.GET)
	public String adminPage(Model model, Principal principal) {

		User loginedUser = (User) ((Authentication) principal).getPrincipal();

		String userInfo = WebUtils.toString(loginedUser);
		model.addAttribute("userInfo", userInfo);

		return Urls.URL_ADMIN_FILE;
	}

	/**
	 * ログインページ
	 *
	 * @param model
	 * @return
	 */
	@RequestMapping(value = Urls.URL_LOGIN_URL, method = RequestMethod.GET)
	public String loginPage(Model model) {
		return Urls.URL_LOGIN_FILE;
	}

	/**
	 * ログアウトページ
	 *
	 * @param model
	 * @return
	 */
	@RequestMapping(value = { Urls.URL_LOGOUT_URL, Urls.URL_LOGOUT_SUCCESS_URL }, method = RequestMethod.GET)
	public String logoutSuccessfulPage(Model model) {
		model.addAttribute("title", "Logout");
		return Urls.URL_LOGOUT_FILE;
	}

	/**
	 * ユーザ情報ページ
	 *
	 * @param model
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = Urls.URL_USERINFO_URL, method = RequestMethod.GET)
	public String userInfo(Model model, Principal principal) {

		// After user login successfully.
		String userName = principal.getName();

		System.out.println("User Name: " + userName);

		User loginedUser = (User) ((Authentication) principal).getPrincipal();

		String userInfo = WebUtils.toString(loginedUser);
		model.addAttribute("userInfo", userInfo);

		return Urls.URL_USERINFO_FILE;
	}

}