package technology.earth.sample001.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import technology.earth.sample001.service.InventoryService;
import technology.earth.sample001.service.MasterProductService;
import technology.earth.sample001.service.StorageLocationService;
import technology.earth.sample001.urls.Urls;

/**
 * 在庫管理
 * @author Owner
 *
 */
@Controller
public class InventoryController {
	@Autowired
	InventoryService inventoryService;

	@Autowired
	MasterProductService masterProductService;

	@Autowired
	StorageLocationService storageLocationService;

	/**
	 * 在庫一覧
	 * @param model
	 * @return
	 */
	@RequestMapping(value = Urls.URL_INVENTORY_LIST_URL, method = RequestMethod.GET)
	public String listInventory(Model model) {
		model.addAttribute("productMap", masterProductService.getProductMap());
		model.addAttribute("storageMap", storageLocationService.getStorageMap());
		model.addAttribute("inventoryList", inventoryService.findAll());
		return Urls.URL_INVENTORY_LIST_FILE;
	}

	/**
	 * 在庫確認（商品毎）
	 * @param productCode
	 * @param model
	 * @return
	 */
	@RequestMapping(value = Urls.URL_INVENTORY_REQUEST_URL, method = RequestMethod.GET)
	public String requestInventory(@RequestParam("productCode") String productCode, Model model) {
		model.addAttribute("product", masterProductService.findByProductCode(productCode));
		model.addAttribute("storageMap", storageLocationService.getStorageMap());
		model.addAttribute("inventoryList", inventoryService.findByProductCode(productCode));
		return Urls.URL_INVENTORY_REQUEST_FILE;
	}

	/**
	 * 在庫確認（保管場所毎）
	 * @param storageLocCode
	 * @param model
	 * @return
	 */
	@RequestMapping(value = Urls.URL_INVENTORY_BY_STORAGE_URL, method = RequestMethod.GET)
	public String inventoryByStorageGet(@RequestParam("storageLocCode") String storageLocCode, Model model) {
		model.addAttribute("storage", storageLocationService.findByStorageLocCode(storageLocCode));
		model.addAttribute("productMap", masterProductService.getProductMap());
		model.addAttribute("inventoryList", inventoryService.findByStorageLoc(storageLocCode));
		return Urls.URL_INVENTORY_BY_STORAGE_FILE;
	}

	/**
	 * 在庫登録（初期状態のGET）
	 * @param model
	 * @return
	 */
	@RequestMapping(value = Urls.URL_CREATE_INVENTORY_URL, method = RequestMethod.GET)
	public String createInventoryGet(Model model) {
		model.addAttribute("productMap", masterProductService.getProductMap());
		model.addAttribute("storageMap", storageLocationService.getStorageMap());
		return Urls.URL_CREATE_INVENTORY_FILE;
	}

	/**
	 * 新規作成（更新の「POST」）
	 * @param productCode
	 * @param storageLocCode
	 * @param quantityStr
	 * @param model
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = Urls.URL_CREATE_INVENTORY_URL, method = RequestMethod.POST)
	public String createInventoryPost(@RequestParam("productCode") String productCode,
			@RequestParam("storageLocCode") String storageLocCode,
			@RequestParam("quantity") String quantityStr, Model model, Principal principal) {
		model.addAttribute("productMap", masterProductService.getProductMap());
		model.addAttribute("storageMap", storageLocationService.getStorageMap());
		String user = principal.getName();
		Integer quantity = Integer.valueOf(quantityStr);
		inventoryService.updateInventory(productCode, storageLocCode, quantity, user);
		return "redirect:" + Urls.URL_INVENTORY_LIST_URL;
	}

}
