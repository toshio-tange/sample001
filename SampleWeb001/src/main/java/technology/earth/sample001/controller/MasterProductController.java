package technology.earth.sample001.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import technology.earth.sample001.service.MasterProductService;
import technology.earth.sample001.urls.Urls;

/**
 * 商品マスタコントローラ
 * @author Owner
 *
 */
@Controller
public class MasterProductController {
	@Autowired
	MasterProductService masterProductService;

	/**
	 * 商品一覧
	 * @param model
	 * @return
	 */
	@RequestMapping(value = Urls.URL_MASTER_PRODUCT_LIST_URL, method = RequestMethod.GET)
	public String listMasterProducts(Model model) {
		model.addAttribute("productList", masterProductService.findAll());
		return Urls.URL_MASTER_PRODUCT_LIST_FILE;
	}

	/**
	 * 新規作成（初期状態の「GET」）
	 * @return
	 */
	@RequestMapping(value = Urls.URL_CREATE_PRODUCT_URL, method = RequestMethod.GET)
	public String createProductGet() {
		return Urls.URL_CREATE_PRODUCT_FILE;
	}

	/**
	 * 新規作成（更新の「POST」）
	 * @param productCode
	 * @param productName
	 * @param model
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = Urls.URL_CREATE_PRODUCT_URL, method = RequestMethod.POST)
	public String createProductPost(@RequestParam("productCode") String productCode,
			@RequestParam("productName") String productName, Model model, Principal principal) {
		String user = principal.getName();
		masterProductService.updateProduct(productCode, productName, user);
		return "redirect:"+Urls.URL_MASTER_PRODUCT_LIST_URL;
	}

	/**
	 * 更新（初期状態のGET）
	 * @param productCode
	 * @return
	 */
	@RequestMapping(value = Urls.URL_EDIT_PRODUCT_URL, method = RequestMethod.GET)
	public String editProductGet(@RequestParam("productCode") String productCode, Model model) {
		model.addAttribute("product", masterProductService.findByProductCode(productCode));
		return Urls.URL_EDIT_PRODUCT_FILE;
	}

	/**
	 * 更新（更新の「POST」）
	 * @param productCode
	 * @param productName
	 * @param model
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = Urls.URL_EDIT_PRODUCT_URL, method = RequestMethod.POST)
	public String editProductPost(@RequestParam("productCode") String productCode,
			@RequestParam("productName") String productName, Model model, Principal principal) {
		String user = principal.getName();

		masterProductService.updateProduct(productCode, productName, user);
		model.addAttribute("product", masterProductService.findByProductCode(productCode));

		return "redirect:"+Urls.URL_MASTER_PRODUCT_LIST_URL;
	}
}
