package technology.earth.sample001.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import technology.earth.sample001.service.StorageLocationService;
import technology.earth.sample001.urls.Urls;

/**
 * 保管場所コントローラ
 * @author Owner
 *
 */
@Controller
public class StorageLocController {
	@Autowired
	StorageLocationService storageLocationService;

	/**
	 * 保管場所一覧
	 * @param model
	 * @return
	 */
	@RequestMapping(value = Urls.URL_STORAGE_LOC_LIST_URL, method = RequestMethod.GET)
	public String listStorageLocs(Model model) {
		model.addAttribute("storageList", storageLocationService.findAll());
		return Urls.URL_STORAGE_LOC_LIST_FILE;
	}

	/**
	 * 新規作成（初期状態の「GET」）
	 * @return
	 */
	@RequestMapping(value = Urls.URL_CREATE_STORAGE_LOC_URL, method = RequestMethod.GET)
	public String createStorageLocGet() {
		return Urls.URL_CREATE_STORAGE_LOC_FILE;
	}

	/**
	 * 新規作成（更新の「POST」）
	 * @param storageLocCode
	 * @param storageLocName
	 * @param model
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = Urls.URL_CREATE_STORAGE_LOC_URL, method = RequestMethod.POST)
	public String createStorageLocPost(@RequestParam("storageLocCode") String storageLocCode,
			@RequestParam("storageLocName") String storageLocName, Model model, Principal principal) {
		String user = principal.getName();
		storageLocationService.updateStorageLoc(storageLocCode, storageLocName, user);
		return "redirect:"+Urls.URL_STORAGE_LOC_LIST_URL;
	}

	/**
	 * 更新（初期状態のGET）
	 * @param storageLocCode
	 * @return
	 */
	@RequestMapping(value = Urls.URL_EDIT_STORAGE_LOC_URL, method = RequestMethod.GET)
	public String editStorageLocGet(@RequestParam("storageLocCode") String storageLocCode, Model model) {
		model.addAttribute("storage", storageLocationService.findByStorageLocCode(storageLocCode));
		return Urls.URL_EDIT_STORAGE_LOC_FILE;
	}

	/**
	 * 更新（更新の「POST」）
	 * @param storageLocCode
	 * @param storageLocName
	 * @param model
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = Urls.URL_EDIT_STORAGE_LOC_URL, method = RequestMethod.POST)
	public String editStorageLocPost(@RequestParam("storageLocCode_1") String storageLocCode,
			@RequestParam("storageLocName") String storageLocName, Model model, Principal principal) {
		String user = principal.getName();
		storageLocationService.updateStorageLoc(storageLocCode, storageLocName, user);
		model.addAttribute("storage", storageLocationService.findByStorageLocCode(storageLocCode));
		return "redirect:"+Urls.URL_STORAGE_LOC_LIST_URL;
	}
}
