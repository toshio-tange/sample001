package technology.earth.sample001.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the master_product database table.
 *
 */
@Entity
@Table(name="master_product")
@NamedQuery(name="MasterProduct.findAll", query="SELECT m FROM MasterProduct m")
public class MasterProduct implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="product_code")
	private String productCode;

	@Column(name="create_at")
	private Timestamp createAt;

	@Column(name="create_user")
	private String createUser;

	@Column(name="effective_finish_date")
	private Timestamp effectiveFinishDate;

	@Column(name="effective_start_date")
	private Timestamp effectiveStartDate;

	@Column(name="product_name")
	private String productName;

	@Column(name="update_at")
	private Timestamp updateAt;

	@Column(name="update_user")
	private String updateUser;

	public MasterProduct() {
	}

	public String getProductCode() {
		return this.productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public Timestamp getCreateAt() {
		return this.createAt;
	}

	public void setCreateAt(Timestamp createAt) {
		this.createAt = createAt;
	}

	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Timestamp getEffectiveFinishDate() {
		return this.effectiveFinishDate;
	}

	public void setEffectiveFinishDate(Timestamp effectiveFinishDate) {
		this.effectiveFinishDate = effectiveFinishDate;
	}

	public Timestamp getEffectiveStartDate() {
		return this.effectiveStartDate;
	}

	public void setEffectiveStartDate(Timestamp effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}

	public String getProductName() {
		return this.productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Timestamp getUpdateAt() {
		return this.updateAt;
	}

	public void setUpdateAt(Timestamp updateAt) {
		this.updateAt = updateAt;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

}