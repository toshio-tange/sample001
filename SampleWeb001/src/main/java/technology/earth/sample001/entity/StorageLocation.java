package technology.earth.sample001.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the storage_location database table.
 *
 */
@Entity
@Table(name = "storage_location")
@NamedQuery(name = "StorageLocation.findAll", query = "SELECT s FROM StorageLocation s")
public class StorageLocation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "storage_loc_code")
	private String storageLocCode;

	@Column(name = "create_at")
	private Timestamp createAt;

	@Column(name = "create_user")
	private String createUser;

	@Column(name = "storage_loc_name")
	private String storageLocName;

	@Column(name = "update_at")
	private Timestamp updateAt;

	@Column(name = "update_user")
	private String updateUser;

	public StorageLocation() {
	}

	public String getStorageLocCode() {
		return this.storageLocCode;
	}

	public void setStorageLocCode(String storageLocCode) {
		this.storageLocCode = storageLocCode;
	}

	public Timestamp getCreateAt() {
		return this.createAt;
	}

	public void setCreateAt(Timestamp createAt) {
		this.createAt = createAt;
	}

	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getStorageLocName() {
		return this.storageLocName;
	}

	public void setStorageLocName(String storageLocName) {
		this.storageLocName = storageLocName;
	}

	public Timestamp getUpdateAt() {
		return this.updateAt;
	}

	public void setUpdateAt(Timestamp updateAt) {
		this.updateAt = updateAt;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
}