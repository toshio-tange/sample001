package technology.earth.sample001.entity;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the inventory database table.
 *
 */
@Embeddable
public class InventoryPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="product_code")
	private String productCode;

	@Column(name="storage_loc_code")
	private String storageLocCode;

	public InventoryPK() {
	}
	public String getProductCode() {
		return this.productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getStorageLocCode() {
		return this.storageLocCode;
	}
	public void setStorageLocCode(String storageLocCode) {
		this.storageLocCode = storageLocCode;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof InventoryPK)) {
			return false;
		}
		InventoryPK castOther = (InventoryPK)other;
		return
			this.productCode.equals(castOther.productCode)
			&& this.storageLocCode.equals(castOther.storageLocCode);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.productCode.hashCode();
		hash = hash * prime + this.storageLocCode.hashCode();

		return hash;
	}
}