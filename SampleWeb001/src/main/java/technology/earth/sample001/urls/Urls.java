package technology.earth.sample001.urls;

public class Urls {
	public static final String URL_ROOT_URL = "/";
	public static final String URL_HOME_URL = "/home";
	public static final String URL_WELCOME_URL = "/welcome";
	public static final String URL_LOGIN_URL = "/login";
	public static final String URL_LOGIN_FILE = "loginPage";
	public static final String URL_ADMIN_URL = "/admin";
	public static final String URL_ADMIN_FILE = "adminPage";
	public static final String URL_LOGOUT_URL = "/logout";
	public static final String URL_LOGOUT_SUCCESS_URL = "/logoutSuccessful";
	public static final String URL_LOGOUT_FILE = "logoutSuccessfulPage";
	public static final String URL_USERINFO_URL = "/userInfo";
	public static final String URL_USERINFO_FILE = "userInfoPage";

	public static final String URL_MASTER_PRODUCT_LIST_URL = "/masterProducts";
	public static final String URL_MASTER_PRODUCT_LIST_FILE = "MasterProductListPage";
	public static final String URL_CREATE_PRODUCT_URL = "/createProduct";
	public static final String URL_CREATE_PRODUCT_FILE = "createProductPage";
	public static final String URL_EDIT_PRODUCT_URL = "/editProduct";
	public static final String URL_EDIT_PRODUCT_FILE = "editProductPage";

	public static final String URL_INVENTORY_LIST_URL = "/inventory";
	public static final String URL_INVENTORY_LIST_FILE = "inventoryListPage";
	public static final String URL_INVENTORY_REQUEST_URL = "/inventoryRequest";
	public static final String URL_INVENTORY_REQUEST_FILE = "inventoryRequestPage";
	public static final String URL_INVENTORY_BY_STORAGE_URL = "/inventoryByStorageLoc";
	public static final String URL_INVENTORY_BY_STORAGE_FILE = "inventoryByStorageLocPage";
	public static final String URL_CREATE_INVENTORY_URL = "/createInventory";
	public static final String URL_CREATE_INVENTORY_FILE = "createInventoryPage";

	public static final String URL_STORAGE_LOC_LIST_URL = "/storageLoc";
	public static final String URL_STORAGE_LOC_LIST_FILE = "StorageLocListPage";
	public static final String URL_CREATE_STORAGE_LOC_URL = "/createStorageLoc";
	public static final String URL_CREATE_STORAGE_LOC_FILE = "createStorageLocPage";
	public static final String URL_EDIT_STORAGE_LOC_URL = "/editStorageLoc";
	public static final String URL_EDIT_STORAGE_LOC_FILE = "editStorageLocPage";
}
